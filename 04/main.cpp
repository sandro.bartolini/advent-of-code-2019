//============================================================================
// Name        : AoC19-04.cpp
// Author      :
// Version     :
// Copyright   : Ing. S. Bartolini
//============================================================================

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <string>
#include <numeric>
#include <sstream>
#include <cstdint>

using Password=int32_t;
using PasswordCyphers=std::vector<int32_t>;
constexpr size_t PWD_LEN=6;
constexpr int32_t MIN_PWD=125730;
constexpr int32_t MAX_PWD=579381;

PasswordCyphers pwd2cyphers(Password pwd)
{
//	std::cout<<"pwd= "<<pwd<<'\n';
	PasswordCyphers pwc; pwc.reserve( PWD_LEN );
	while( pwd != 0 )
	{
		pwc.push_back( pwd % 10 );
		pwd /=10;
	}
	assert( pwc.size() == PWD_LEN );
//	std::copy(std::begin(pwc), std::end(pwc),std::ostream_iterator<int32_t>(std::cout," "));
//	std::cout<<'\n';
	return pwc;
}

int32_t calcFirstAnswer( )
{
	int32_t numPwds{0};
	for(int p= MIN_PWD; p <= MAX_PWD; ++p)
	{
		PasswordCyphers pwc= pwd2cyphers(p);
		if( !std::is_sorted( std::rbegin(pwc), std::rend(pwc) ) )
			continue;
		if( std::adjacent_find( std::begin(pwc), std::end(pwc) ) == std::end(pwc) )
			continue;
		//else
//		std::cout<<"Got one!"<<'\n';
		++numPwds;
	}
	return numPwds;
}


int32_t calcSecondAnswer( )
{
	int32_t numPwds{0};
	for(int p= MIN_PWD; p <= MAX_PWD; ++p)
	{
		PasswordCyphers pwc= pwd2cyphers(p);
		if( !std::is_sorted( std::rbegin(pwc), std::rend(pwc) ) )
			continue;
		if( std::adjacent_find( std::begin(pwc), std::end(pwc) ) == std::end(pwc) )
			continue;

		bool possibleMatch=true;
		bool match=false;
		auto itBeg=std::rbegin(pwc);
		while( possibleMatch && !match )
		{
			auto it= std::adjacent_find( itBeg, std::rend(pwc) ); 
			if( it != std::rend(pwc) )
			{
				auto endRep= std::upper_bound(it,pwc.rend(),*it);
				int32_t numRepetitions= endRep - it;
				if( numRepetitions == 2 )
					match= true;
				itBeg= it+numRepetitions; 
			}
			else
				possibleMatch= false;	
		}
		if( !possibleMatch )
			continue;	
		assert( match==true );
//		std::cout<<"Got one!"<<'\n';
		++numPwds;
	}
	return numPwds;
}



// ###################################################### utility 


int main() {

	// 1 ---------------------------------------------------------------
	std::cout<<"Number of passwords (1)= "<< calcFirstAnswer() << '\n';

	// 2 ---------------------------------------------------------------
	std::cout<<"\n\n";

	std::cout<<"Number of passwords (2)= "<< calcSecondAnswer() << '\n';


	std::cout.flush();
	return 0;
}
