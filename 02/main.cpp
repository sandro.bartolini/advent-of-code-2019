//============================================================================
// Name        : AoC19-02.cpp
// Author      :
// Version     :
// Copyright   : Ing. S. Bartolini
//============================================================================

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <string>
#include <numeric>

const std::string inFile="input1.txt";

using Value=int32_t;


using ProgramMemory=std::vector<Value>;

ProgramMemory calcFirstAnswer( ProgramMemory const& theProgram )
{
	ProgramMemory program{theProgram};
	uint32_t ip{0};
	constexpr int INSTR_LEN=4;
	uint32_t opcode{0};
	do
	{
		assert( ip < std::size(program) );
		opcode= program[ip];
		int idx1, idx2,idxOut;
		switch(opcode)
		{
		case 99:
			break;
		case 1:
		case 2:
			idx1= program[ip+1];
			idx2= program[ip+2];
			idxOut= program[ip+3];
			program[ idxOut ]= (opcode==1) ? (program[idx1] + program[idx2]) : (program[idx1] * program[idx2]);
			ip+= INSTR_LEN;
			break;
		default:
			assert(0);
			break;
		};
	}
	while(opcode != 99);

	return program;
}

Value calcSecondAnswer( ProgramMemory const& theProgram )
{
	Value MAGIC_OUTPUT_VALUE=19690720;
	int val1,val2;
	for(val2=0; val2 <=99 ; ++val2)
	{
		for(val1=0; val1 <=99 ; ++val1)
		{
			auto tmpProgram{theProgram};
			tmpProgram[1]= val1;
			tmpProgram[2]= val2;
			if( calcFirstAnswer(tmpProgram)[0] == MAGIC_OUTPUT_VALUE )
				return val1*100 + val2;
		}
	}
	assert(0);
	return 0;
}

// ###################################################### Test
// small test structures
using Programs=std::vector<ProgramMemory>;
Programs testIn = {  {1,9,10,3,2,3,11,0,99,30,40,50}
					,{1,0,0,0,99}
					,{2,3,0,3,99}
					,{2,4,4,5,99,0}
					,{1,1,1,4,99,5,6,0,99}
					};
Programs testExpectedOut1 = { {3500,9,10,70,2,3,11,0,99,30,40,50}
							,{2,0,0,0,99}
							,{2,3,0,6,99}
							,{2,4,4,5,99,9801}
							,{30,1,1,4,2,5,6,0,99}
					};

std::ostream& operator<<(std::ostream& os, ProgramMemory const& prog)
{
	assert( prog.size() > 1 );
	std::copy(std::begin(prog), std::end(prog)-1,std::ostream_iterator<Value>(os,","));
	os<<prog.back();
	return os;
}


void doSmallTests1( Programs const& ins, Programs const& expectedOuts)
{
	assert( ins.size() == expectedOuts.size() );
	for( size_t i= 0; i < ins.size(); ++i)
	{
		ProgramMemory const out= calcFirstAnswer( ProgramMemory{ins[i]} );
		std::cout<<"In= "<<ins[i]<<"\nout= "<< out <<"\nexpectedOut= "<<expectedOuts[i]<<"\n"<< ( (out == expectedOuts[i]) ? "OK!" : "NO !!!")<<"\n\n";
	}
}


// ######################################################
int main() {

	// 1 ---------------------------------------------------------------
		// small tests
	doSmallTests1(testIn,testExpectedOut1);
	std::cout<<"\n";

		// the big one
	std::ifstream ifs( inFile.c_str() );
	assert( ifs );

	ProgramMemory program;
	{
		char comma;
		Value val;
		while( ifs>>val )
		{
			program.push_back(val);
			ifs>>comma;
		}
	}
	std::copy( std::istream_iterator<Value>(ifs), std::istream_iterator<Value>(), std::back_inserter(program) );
	ifs.close();

	program[1]= 12;
	program[2]= 2;

	std::cout<<"Initial program= "<< program << '\n';

	std::cout<<"Final program status(1)= "<< calcFirstAnswer(program)[0] << '\n';

	// 2 ---------------------------------------------------------------
	std::cout<<"\n\n";

		// the big one
	std::cout<<"Magic noun-verb (2)= "<< calcSecondAnswer(program) << '\n';


	std::cout.flush();
	return 0;
}
