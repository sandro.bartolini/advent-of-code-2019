//============================================================================
// Name        : AoC19-03.cpp
// Author      :
// Version     :
// Copyright   : Ing. S. Bartolini
//============================================================================

#include <iostream>
#include <iomanip>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <string>
#include <numeric>
#include <sstream>
#include <cstdint>

const std::string inFile="input1.txt";
//const std::string inFile="test0.txt";
//const std::string inFile="test1.txt";
//const std::string inFile="test2.txt";

struct Pos
{
	int32_t x;	
	int32_t y;	
};
bool operator==(Pos const& p1, Pos const& p2)
{
	return p1.x==p2.x && p1.y==p2.y ;
}

constexpr int32_t intAbs(int32_t const val){ return (val>=0) ? val : -val; }

using WirePositions=std::vector<Pos>;


int32_t calcFirstAnswer( WirePositions const& wp1, WirePositions const& wp2 )
{
	int32_t minDist= std::numeric_limits<int32_t>::max();
	for(auto const& p1 : wp1)
	{
		for(auto const& p2 : wp2)   // Not very efficient but it works
		{
			if( p1 == p2 )
			{
				int32_t const dist= intAbs(p1.x) + intAbs(p1.y);
				if( minDist > dist )
					minDist= dist;
			}
		}
	}
	return minDist;
}

int32_t pathLenToPoint(WirePositions const& wp, Pos const& targetPos)
{
	int32_t dist{0};
	bool foundPos=false;
	Pos lastPos{0,0};
	for(auto const& p : wp)
	{
		dist+= intAbs(p.x - lastPos.x) + intAbs(p.y - lastPos.y);
		if( p == targetPos ) 
		{
			foundPos= true;
			break; // !!
		}
		lastPos= p;
	}
	assert( foundPos );
	return dist;
}

int32_t calcSecondAnswer( WirePositions const& wp1, WirePositions const& wp2 )
{
	int32_t minDist= std::numeric_limits<int32_t>::max();
	for(auto const& p1 : wp1)
	{
		for(auto const& p2 : wp2)   // Not very efficient but it works
		{
			if( p1 == p2 )
			{
				int32_t const dist= pathLenToPoint(wp1,p1) + pathLenToPoint(wp2,p1);
				if( minDist > dist )
					minDist= dist;
			}
		}
	}
	return minDist;
}



// ###################################################### utility 


std::ostream& operator<<(std::ostream& os, Pos const& pos)
{
	os<<'('<<pos.x<<", "<<pos.y<<')';
	return os;
}

std::ostream& operator<<(std::ostream& os, WirePositions const& wp)
{
	assert( wp.size() > 1 );
	std::copy(std::begin(wp), std::end(wp),std::ostream_iterator<Pos>(os,","));
	return os;
}


WirePositions addSegment(WirePositions&& wp, char const dir, int32_t const steps)
{
	Pos lastPos= (wp.size()!=0) ? wp.back() : Pos{0,0};
	for(int i= 0; i < steps; ++i)
	{
		switch( dir )
		{
			case 'R': ++lastPos.x; break;
			case 'L': --lastPos.x; break;
			case 'D': --lastPos.y; break;
			case 'U': ++lastPos.y; break;
		};
//		std::cout<<lastPos<<" ";
		wp.push_back( lastPos );
	}
//	std::cout<<'\n';
	return wp;
}

WirePositions decodeStrPath(std::string const& strWirePosInput)
{
	WirePositions wp;
//	std::cout<<"stringPath= "<<strWirePosInput<<'\n';
	std::stringstream ss(strWirePosInput);
	char comma;
	char dir;
	int32_t steps;
	while( ss>>dir>>steps )
	{
//		std::cout<<"Dir= "<<dir<<", steps= "<<steps<<'\n';
		wp= addSegment(std::move(wp),dir,steps);
//		std::cout<<"wirepathTillNow= "<<wp<<'\n';
		ss >> comma;
	}
	return wp;	
}

// ######################################################
int main() {

	// 1 ---------------------------------------------------------------
	std::ifstream ifs( inFile.c_str() );
	assert( ifs );

	WirePositions wirePos1;
	WirePositions wirePos2;
	{
		std::string strWirePosInput;
		std::getline(ifs, strWirePosInput);
		wirePos1= decodeStrPath(strWirePosInput);
		std::getline(ifs, strWirePosInput);
		wirePos2= decodeStrPath(strWirePosInput);
	}
	ifs.close();

	std::cout<<"Minimum distance (1)= "<< calcFirstAnswer(wirePos1,wirePos2) << '\n';

	// 2 ---------------------------------------------------------------
	std::cout<<"\n\n";

	std::cout<<"Minimum distance (2)= "<< calcSecondAnswer(wirePos1,wirePos2) << '\n';


	std::cout.flush();
	return 0;
}
