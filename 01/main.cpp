//============================================================================
// Name        : AoC19-01.cpp
// Author      :
// Version     :
// Copyright   : Ing. S. Bartolini
//============================================================================

#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <cassert>
#include <string>
#include <numeric>

const std::string inFile="input1.txt";

using Value=int32_t;

Value massToFuel(Value const mass)
{
	Value fuel= mass/3 - 2;
	if( fuel >= 0 )
		return fuel;
	else
		return 0;
}

Value fuelForFuel(Value const initialFuel)
{
	Value tmpFuel{initialFuel};
	Value ffiTot{0};
	do
	{
		tmpFuel= massToFuel(tmpFuel);
	}
	while( ffiTot+= tmpFuel, tmpFuel > 0 );

	return ffiTot;
}

using Masses=std::vector<Value>;

Value calcFirstAnswer( Masses const& masses )
{
	Value const totalFuel= std::accumulate( std::begin(masses), std::end( masses ), Value{0}, []( Value const parzSum, Value const mass_i )
		{
			Value const fuel_i= massToFuel(mass_i);
			assert( fuel_i >= 0 );
			return parzSum + fuel_i;
		});
	return totalFuel;
}

Value calcSecondAnswer( Masses const& masses )
{
	Value const totalFuel= std::accumulate( std::begin(masses), std::end( masses ), Value{0}, []( Value const parzSum, Value const mass_i )
		{
			Value const fuel_i= massToFuel(mass_i);
			assert( fuel_i >= 0 );

			Value const fuelForFuel_i= fuelForFuel(fuel_i);

			return parzSum + fuel_i + fuelForFuel_i;
		});
	return totalFuel;
}


// ###################################################### Test
// small test structures
Masses testIn = {12, 14, 1969, 100756};
using Fuels=std::vector<Value>;
Masses testExpectedOut1 = {2, 2, 654, 33583};
Masses testExpectedOut2 = {2, 2, 966, 50346};

void doSmallTests1( Masses const& ins, Masses const& expectedOuts)
{
	assert( ins.size() == expectedOuts.size() );
	for( size_t i= 0; i < ins.size(); ++i)
	{
		Value const out= calcFirstAnswer( Masses(1,ins[i]) );
		std::cout<<"In= "<<ins[i]<<", out= "<< out <<", expectedOut= "<<expectedOuts[i]<<" "<< ( (out == expectedOuts[i]) ? "OK!" : "NO !!!")<<'\n';
	}
}

void doSmallTests2( Masses const& ins, Masses const& expectedOuts)
{
	assert( ins.size() == expectedOuts.size() );
	for( size_t i= 0; i < ins.size(); ++i)
	{
		Value const out= calcSecondAnswer( Masses(1,ins[i]) );
		std::cout<<"In= "<<ins[i]<<", out= "<< out <<", expectedOut= "<<expectedOuts[i]<<" "<< ( (out == expectedOuts[i]) ? "OK!" : "NO !!!")<<'\n';
	}
}


// ###################################################### Test
int main() {

	// 1 ---------------------------------------------------------------
		// small tests
	doSmallTests1(testIn,testExpectedOut1);
	std::cout<<"\n";

		// the big one
	std::ifstream ifs( inFile.c_str() );
	assert( ifs );

	Masses masses;
	std::copy( std::istream_iterator<Value>(ifs), std::istream_iterator<Value>(), std::back_inserter(masses) );
	ifs.close();

	//std::copy( std::begin(masses), std::end( masses ), std::ostream_iterator<Value>(std::cout, " "));
	//std::cout<<'\n';

	std::cout<<"Total_fuel(1)= "<< calcFirstAnswer(masses) << '\n';

	// 2 ---------------------------------------------------------------
	std::cout<<"\n\n";
		// small tests
	doSmallTests2(testIn,testExpectedOut2);
	std::cout<<"\n";

		// the big one

	std::cout<<"Total_fuel(2)= "<< calcSecondAnswer(masses) << '\n';


	std::cout.flush();
	return 0;
}
